-- liquibase formatted sql

-- changeset emo.leumassi:user_job
CREATE TABLE IF NOT EXISTS user_job(
    user_id INT NOT NULL,
    job_id INT NOT NULL,
    PRIMARY KEY (user_id, job_id)
)
