package com.kazi.core.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@Table
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id", nullable = false)
    private int messageId;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String body;

    @Column
    private Date sentDate;

    @ManyToOne(targetEntity = Thread.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "thread", referencedColumnName = "thread_id")
    private Thread thread;
}
