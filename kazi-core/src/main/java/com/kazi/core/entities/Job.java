package com.kazi.core.entities;

import com.kazi.enums.JobStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table
public class Job implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "job_id", nullable = false)
    private int jobId;

    @Column(columnDefinition = "VARCHAR(50)", nullable = false)
    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column(columnDefinition = "int")
    private int salary;

    @Column
    private Date creationDate;

    @Column(columnDefinition = "VARCHAR(50)")
    private String category;

    @Column(columnDefinition = "VARCHAR(50)")
    private String location;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "VARCHAR(10)", nullable = false)
    private JobStatus status;

    @ManyToMany(mappedBy = "jobs", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<User> users = new ArrayList<>();

    @OneToMany(mappedBy = "job", targetEntity = Review.class, fetch = FetchType.LAZY)
    private List<Review> reviews = new ArrayList<>();
}
