package com.kazi.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kazi.enums.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Table
@Entity
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    private int userId;

    @Column(columnDefinition = "VARCHAR(50)", unique = true, nullable = false)
    private String username;

    @Column(columnDefinition = "VARCHAR(50)", unique = true)
    private String email;

    @Column(columnDefinition = "BIT")
    private boolean enabled;

    @Column(columnDefinition = "VARCHAR(255)", nullable = false)
    private String password;

    @Column
    private Date firstLogin;

    @Column
    private Date lastLogin;

    @Column(columnDefinition = "VARCHAR(50)")
    private String firstName;

    @Column(columnDefinition = "VARCHAR(50)")
    private String lastName;

    @Column(columnDefinition = "VARCHAR(50)")
    private String phoneNumber;

    @Column(columnDefinition = "BIT", nullable = false)
    private boolean blocked;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "VARCHAR(10)")
    private Role role;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "user_job",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "job_id"))
    private List<Job> jobs = new ArrayList<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "user_thread",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "thread_id"))
    private List<Thread> threads = new ArrayList<>();
}
