package com.kazi.enums;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    USER, ADMIN, PRO;

    /*public static Role getRoleByName(final String name) throws IllegalAccessException {
        for (Role role : Role.values()) {
            if (name.equals(role.getAuthority())) {
                return role;
            }
        }
        throw new IllegalAccessException("No Role for name: " + name);
    }*/

    @Override
    public String getAuthority() {
        return name();
    }
}
