FROM adoptopenjdk/openjdk11
WORKDIR /opt
ENV PORT 8080
EXPOSE 8080
COPY kazi-core/target/*-exec.jar /opt/kazi.jar
ENTRYPOINT ["java", "-jar", "kazi.jar"]