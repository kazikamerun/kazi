#! /bin/bash

KAZI_POD=$(kubectl get pod -l app=kazi-k8s -o jsonpath="{.items[0].metadata.name}")

echo "Start POD $KAZI_POD\n"

kubectl exec -it $KAZI_POD  -- /bin/bash

