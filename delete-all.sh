#! /bin/bash

echo "delete kazi-service"
kubectl delete service kazi-service

echo "\ndelete kazi-credentials"
kubectl delete secret kazi-credentials

echo "\ndelete kazi-configmap"
kubectl delete configmap kazi-config

echo "\ndelete kazi-deployment"
kubectl delete -n default deployment kazi-deployment

#echo "\nstop kazi-k8s docker container"
#docker stop $(docker ps -a -q --filter "ancestor=kazi-k8s")

#echo "\nremove kazi-k8s docker container"
#docker rm $(docker ps -a -q --filter "ancestor=kazi-k8s")

echo "\nremove kazi-k8s docker image"
docker rmi kazi-k8s
##docker ps -q --filter ancestor="kazi-k8s" | xargs -r docker stop