#! /bin/bash

docker build -t kazi-k8s .

kubectl apply -f deployment/service.yaml

kubectl apply -f deployment/secret.yaml

kubectl apply -f deployment/configmap.yaml

kubectl apply -f deployment/deployment.yaml