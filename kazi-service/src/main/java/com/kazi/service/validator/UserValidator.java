package com.kazi.service.validator;


import com.kazi.api.details.UserDetails;
import com.kazi.api.service.UserService;
import com.kazi.service.utils.RegistrationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return UserDetails.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        final UserDetails details = (UserDetails) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        if (userService.findByUsername(details.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        if (RegistrationUtil.validateEmail(details.getEmail())) {
            errors.rejectValue("email", "email.no.correct");
        }

        if (userService.findByEmail(details.getEmail()) != null) {
            errors.rejectValue("email", "Duplicate.userForm.email");
        }
    }
}
