package com.kazi.service.impl;

import com.kazi.api.details.RegistrationDetails;
import com.kazi.api.details.UserDetails;
import com.kazi.api.mapper.UserMapper;
import com.kazi.api.mapper.UserMapperImpl;
import com.kazi.api.model.MailModel;
import com.kazi.api.repository.UserRepository;
import com.kazi.api.service.RegistrationService;
import com.kazi.api.service.UserService;
import com.kazi.core.entities.User;
import com.kazi.service.security.MyPasswordEncoder;
import com.kazi.service.utils.RegistrationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Transactional
@Service("userServiceImpl")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RegistrationService registrationService;
    private final MyPasswordEncoder passwordEncoder = new MyPasswordEncoder();
    private final UserMapper userMapper = new UserMapperImpl();

    @Override
    public MailModel create(final UserDetails userDetails) {
        final User user = userMapper.mapUserDetails(userDetails);
        Date today = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
        user.setFirstLogin(today);
        user.setPassword(passwordEncoder.getBCryptPasswordEncoder().encode(userDetails.getPassword()));
        userRepository.save(user);

        final RegistrationDetails registration = new RegistrationDetails();
        registration.setUserId(user.getUserId());
        final String token = registrationService.create(registration);
        return MailModel.builder()
                .to(userDetails.getEmail())
                .subject("Welcome")
                .content(RegistrationUtil.createRegistrationMail(token))
                .build();
    }

    @Override
    public UserDetails findByUsername(String username) {
        final User user = userRepository.findByUsername(username);
        return userMapper.mapUser(user);
    }

    @Override
    public UserDetails findByEmail(String email) {
        final User user = userRepository.findByEmail(email);
        return userMapper.mapUser(user);
    }
}
