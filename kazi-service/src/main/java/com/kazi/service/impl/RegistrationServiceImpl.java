package com.kazi.service.impl;

import com.kazi.api.details.RegistrationDetails;
import com.kazi.api.mapper.RegistrationMapper;
import com.kazi.api.mapper.RegistrationMapperImpl;
import com.kazi.api.repository.RegistrationRepository;
import com.kazi.api.service.RegistrationService;
import com.kazi.core.entities.Registration;
import com.kazi.service.utils.RegistrationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private RegistrationRepository repository;
    private final RegistrationMapper registrationMapper = new RegistrationMapperImpl();

    @Override
    public String create(RegistrationDetails registrationDetails) {
        final String token = RegistrationUtil.createToken();
        registrationDetails.setToken(token);
        registrationDetails.setExpirationDate(RegistrationUtil.calculateExpiryDate());
        final Registration registration = registrationMapper.mapRegistrationDetails(registrationDetails);
        repository.save(registration);
        return token;
    }

    @Override
    public boolean validateUrlToken(String token) {
        return true;
    }
}
