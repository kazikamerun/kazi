package com.kazi.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.core.io.InputStreamSource;

@Getter
@AllArgsConstructor
@Builder
public class MailModel {

    private final String to;
    private final String subject;
    private final String content;
    private final String attachmentFilename;
    private final InputStreamSource inputStreamSource;
}
