package com.kazi.api.mapper;

import com.kazi.api.details.RegistrationDetails;
import com.kazi.core.entities.Registration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper
public interface RegistrationMapper {

    void map(@MappingTarget Registration registration, final RegistrationDetails registrationDetails);

    RegistrationDetails mapRegistration(Registration registration);

    @Mapping(source = "userId", target = "user.userId")
    Registration mapRegistrationDetails(RegistrationDetails registrationDetails);
}
