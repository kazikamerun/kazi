package com.kazi.api.mapper;

import com.kazi.api.details.UserDetails;
import com.kazi.core.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface UserMapper {

    UserDetails mapUser(User user);

    @Mapping(target = "firstLogin", ignore = true)
    User mapUserDetails(UserDetails userDetails);
}
