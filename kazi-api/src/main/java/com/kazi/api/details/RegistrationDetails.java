package com.kazi.api.details;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RegistrationDetails {

    private String registrationId;
    private String token;
    private Integer userId;
    private Date expirationDate;
}
