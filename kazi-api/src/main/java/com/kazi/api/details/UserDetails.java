package com.kazi.api.details;

import com.kazi.enums.Role;

import java.util.Date;

public class UserDetails  {

    private int userId;
    private String username;
    private String email;
    private boolean enabled;
    private String password;
    private Date firstLogin;
    private Date lastLogin;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private boolean blocked;
    private String description;
    private Role role;

    public UserDetails() {
    }

    public UserDetails(UserDetails details) {
        this.username = details.getUsername();
        this.role = details.getRole();
        this.username = details.getUsername();
        this.email = details.getEmail();
        this.enabled = details.isEnabled();
        this.blocked = details.isBlocked();
        this.password = details.getPassword();
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(Date firstLogin) {
        this.firstLogin = firstLogin;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    /*public List<RoleDetails> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleDetails> roles) {
        this.roles = roles;
    }

    public List<JobDetails> getJobs() {
        return jobs;
    }

    public void setJobs(List<JobDetails> jobs) {
        this.jobs = jobs;
    }

    public List<ThreadDetails> getThreads() {
        return threads;
    }

    public void setThreads(List<ThreadDetails> threads) {
        this.threads = threads;
    }*/

    @Override
    public String toString() {
        return "UserDetails{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", enabled=" + enabled +
                ", firstLogin='" + firstLogin + '\'' +
                ", lastLogin='" + lastLogin + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", blocked=" + blocked +
                ", description='" + description + '\'' +
                /*", roles=" + roles +
                ", jobs=" + jobs +
                ", threads=" + threads +*/
                '}';
    }
}
