package com.kazi.api.details;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewDetails {

    private int reviewId;
    private String title;
    private String description;
    private String reviewDate;
    private int note;
}
