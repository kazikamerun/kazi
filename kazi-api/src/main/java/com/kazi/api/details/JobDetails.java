package com.kazi.api.details;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobDetails {

    private int jobId;
    private String title;
    private String description;
    private int salary;
    private String creationDate;
    private String category;
    private String location;
    private int status;
}
