package com.kazi.api.service;

import com.kazi.api.details.UserDetails;
import com.kazi.api.model.MailModel;

public interface UserService {

    MailModel create(UserDetails userDetails);

    UserDetails findByUsername(String username);

    UserDetails findByEmail(String email);
}
