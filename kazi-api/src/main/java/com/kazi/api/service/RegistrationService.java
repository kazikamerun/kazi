package com.kazi.api.service;

import com.kazi.api.details.RegistrationDetails;

public interface RegistrationService {

    String create(final RegistrationDetails registrationDetails);

    boolean validateUrlToken(final String url);
}
